"""Execute commands on shell."""

import subprocess  # noqa: S404
from datetime import datetime
from typing import Tuple

UTF8 = "UTF-8"


def execute_command(command: str) -> Tuple[int, str, str, datetime, datetime]:
    """Execute a command on the shell.

    :param command: String command
    :returns: Tuple[exit_code, stdout, stderr, start, end]
    """
    start_timestamp = datetime.now()
    proc = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,  # noqa: S602
    )
    stdout, stderr = proc.communicate()
    end_timestamp = datetime.now()

    return (
        int(proc.returncode),
        stdout.decode(UTF8),
        stderr.decode(UTF8),
        start_timestamp,
        end_timestamp,
    )
