"""Create documentation from specific yaml file comments."""

import os
import re
from typing import List, Union

import ruamel.yaml as ryaml  # noqa: WPS301
import structlog

logger = structlog.get_logger()

LIST_OF_COMMENT_TOKENS = List[ryaml.tokens.CommentToken]
COMMENT_TOKEN = ryaml.tokens.CommentToken
TOKEN_LIST_LIST_TYPES = Union[LIST_OF_COMMENT_TOKENS, COMMENT_TOKEN, None]
# "ruamel.yaml" drives me nuts... WTF?

INDEX_FILE = "index.rst"


class YamlDoc(object):
    """Documentation Parser for yaml files."""

    def __init__(
        self,
        generate_if_uncommented: bool = False,
        include_folder_doc: Union[str, None] = None,
    ) -> None:
        """Initialize class.

        :param include_folder_doc: Path for specific file in
                                   each folder containing folder
                                   description.
        :param generate_if_uncommented: Generate output even if there are no comments
                                  in the file.
        """
        self.include_folder_doc = include_folder_doc
        self.generate_if_uncommented = generate_if_uncommented

    @staticmethod
    def has_comment(cmap: ryaml.comments.CommentedMap) -> bool:
        """Check if CommentedMap has comments for processing.

        :param cmap: The commented map.
        :returns: True if comments available, False otherwise.
        """
        if not hasattr(cmap, "ca") or not cmap.ca:  # noqa: WPS421
            return False

        has_comment = hasattr(cmap.ca, "comment") and bool(  # noqa: WPS421
            cmap.ca.comment
        )
        has_items = hasattr(cmap.ca, "items") and bool(cmap.ca.items)  # noqa: WPS421
        return has_comment or has_items

    @staticmethod
    def doc_string_from_list_lists_of_comment_tokens(
        token_lists: List[TOKEN_LIST_LIST_TYPES], root: bool
    ) -> str:
        """Generate description out of a list CommentTokens and lists of CommentTokens.

        :param token_lists: List of tokens, token_lists and None
        :param root: Set true if root.

        :returns: description collected from all CommentTokens
        """
        description = ""

        if not token_lists:
            return ""

        for token_list in token_lists:
            if not token_list:
                continue

            description += YamlDoc.doc_string_from_comment_tokens(token_list, root)

        return description

    @staticmethod
    def doc_string_from_comment_tokens(
        tokens: List[ryaml.tokens.CommentToken], root: bool
    ) -> str:
        """Create documentation out of comments.

        :param tokens: The list of tokens for the current entry.
        :param root: True if in document root.
        :returns: Documentation from comments
        """
        documentation = ""

        if not isinstance(tokens, list):
            tokens = [tokens]

        for token in tokens:
            if not token:
                continue

            stripped_value = token.value.strip()
            if not stripped_value.startswith("###"):
                continue
            indent = ""
            if not root:
                indent = " " * 3

            stripped_value = re.sub("^### ?", "", stripped_value)
            documentation += "{}{}\n".format(indent, stripped_value)
        return documentation

    @staticmethod
    def collect_documentation_from_items(  # noqa: WPS210
        entry: Union[ryaml.comments.CommentedMap, ryaml.comments.CommentedSeq],
        yaml_path: str,
    ) -> str:
        """Collect documentation from items.

        :param yaml_path: The current yaml path
        :param entry: The current entry
        :returns: Documentation for items
        """
        documentation = ""
        if entry.ca.items:
            is_array = isinstance(entry, ryaml.comments.CommentedSeq)

            for sub_path, token_lists in entry.ca.items.items():
                dot = "" if yaml_path == "" else "."
                yaml_sub_path = (
                    f"{yaml_path}[{sub_path}]"
                    if is_array
                    else f"{yaml_path}{dot}{sub_path}"
                )

                description = YamlDoc.doc_string_from_list_lists_of_comment_tokens(
                    token_lists, root=False
                )
                if description:
                    documentation += "\n- **{}**\n{}\n".format(
                        yaml_sub_path, description
                    )

        return documentation

    @staticmethod
    def doc_yaml(  # noqa: WPS210, WPS231
        entry: Union[ryaml.comments.CommentedMap, str, dict, list, int, float, bool],
        yaml_path: str = "",
    ) -> str:
        """Document yaml content.

        :param entry: CommentedMap Entry
        :param yaml_path: The parent yaml_path (not to be filled by caller)
        :returns: Documentation
        """
        documentation = ""
        root = yaml_path == ""
        if YamlDoc.has_comment(entry):
            if root:
                comments = YamlDoc.doc_string_from_list_lists_of_comment_tokens(
                    entry.ca.comment,
                    root
                )
                documentation += "{}\n\n".format(comments)
            documentation += YamlDoc.collect_documentation_from_items(entry, yaml_path)

        if isinstance(entry, dict):
            for key, dict_value in entry.items():
                documentation += YamlDoc.doc_yaml(
                    dict_value, f"{yaml_path}.{key}" if yaml_path else key
                )
        elif isinstance(entry, list):
            for index, dict_value2 in enumerate(entry):
                documentation += YamlDoc.doc_yaml(dict_value2, f"{yaml_path}[{index}]")

        return documentation

    def doc_yaml_file(self, path: str, output_path: str) -> bool:  # noqa: WPS210
        """Create documentation for yaml file.

        :param path: the file path
        :param output_path: The file output path
        :returns: True if file generated, False otherwise
        """
        basename = os.path.basename(path)
        abs_path = os.path.relpath(path, os.path.dirname(output_path))
        file_link = f".. literalinclude:: {abs_path}\n   :language: yaml\n\n"

        documentation = ""
        with open(path, "r") as yaml_file:
            yaml_parser = ryaml.YAML(typ="rt")
            yaml_parser.preserve_quotes = True
            yaml_data = yaml_parser.load(yaml_file)
            documentation = YamlDoc.doc_yaml(yaml_data)

        if documentation or self.generate_if_uncommented:
            with open(output_path, "w") as output_file:
                output_file.write(
                    "{}\n{}\n{}\n{}\n\n".format(
                        basename, "-" * len(basename), documentation, file_link
                    )
                )
            return True
        return False

    def process_current_directory_files(
        self, root: str, out_root: str, filenames: List[str]
    ) -> List[str]:
        """Create yaml documentation files in current directory.

        :param root: The input root dir
        :param out_root: The output root dir
        :param filenames: The list of files
        :returns: List of created files
        """
        created_files = []
        for filename in filenames:
            if filename.endswith((".yaml", ".yml")):
                out_path = os.path.join(out_root, "{}.rst".format(filename))
                created = self.doc_yaml_file(os.path.join(root, filename), out_path)

                if created:
                    created_files.append(out_path)

        return created_files

    def create_directory_index(  # noqa: WPS210, WPS211
        self,
        root: str,
        out_root: str,
        sub_directories: List[str],
        created_files: List[str],
        overwrite_headline: Union[str, None],
    ) -> None:
        """Create index.rst for directory.

        :param root: The current directory
        :param out_root: The directory to create the index for
        :param sub_directories: The list of subdirectories
                                searched for other index files
        :param created_files: The list of files to add
        :param overwrite_headline: Overwrite the Headline
        """
        index_content = ""
        if not overwrite_headline:
            overwrite_headline = out_root.split("/", 1)[-1]

        overwrite_headline = "{}\n{}\n\n".format(
            overwrite_headline, "=" * len(overwrite_headline)
        )

        dir_doc = self._collect_directory_doc(root)
        if dir_doc:
            overwrite_headline = dir_doc

        index_content += self._index_collect_subdirs(out_root, sub_directories)
        index_content += self._index_collect_files(created_files)

        if index_content or dir_doc:
            with open(os.path.join(out_root, INDEX_FILE), "w") as idxfile:
                idxfile.write(overwrite_headline)
                idxfile.write(index_content)

    def doc_directory(  # noqa: WPS210
        self, path: str, output_path: str, headline: str
    ) -> None:
        """Create yaml documentation for a directory.

        :param path: The path the document shall be extracted from
        :param output_path: The path to output the documentation to
        :param headline: The main headline for the documentation.
        """
        walk = os.walk(path, topdown=False)  # Important for index generation.
        for root, directories, filenames in walk:
            out_root = root.replace(path, output_path, 1)
            os.makedirs(out_root, exist_ok=True)
            created_files = self.process_current_directory_files(
                root, out_root, filenames
            )
            self.create_directory_index(
                root,
                out_root,
                directories,
                created_files,
                headline if path == root else None,
            )

    @staticmethod
    def _index_collect_subdirs(out_root: str, sub_directories: List[str]) -> str:
        """Collect all index files from sub directories.

        :param out_root: Current directory
        :param sub_directories: The sub directories
        :returns: List of sub directories for index
        """
        index_content = ""

        for sub_dir in sub_directories:
            if os.path.isfile(os.path.join(out_root, sub_dir, INDEX_FILE)):
                index_content += "   {}/{}\n".format(sub_dir, INDEX_FILE)

        if index_content:
            return (
                "\n.. toctree::\n"
                "   :maxdepth: 1\n"
                "   :caption: Subdirectories:\n\n" "{}"
            ).format(index_content)

        return ""

    @staticmethod
    def _index_collect_files(created_files: List[str]) -> str:
        """Collect all entries for files for index file.

        :param created_files: List of created files
        :return: Content for index file
        """
        index_content = ""
        for created_file in created_files:
            index_content += "   {}\n".format(
                os.path.basename(created_file)
            )

        if index_content:
            return (
                "\n.. toctree::\n"
                "   :maxdepth: 1\n"
                "   :caption: Files:\n\n" "{}"
            ).format(index_content)

        return ""

    def _collect_directory_doc(self, root: str) -> str:
        """Collect documentation file in current directory.

        :param root: Current directory
        :returns: Content of documenation file in directory
        """
        if not self.include_folder_doc:
            return ""

        doc_path = os.path.join(root, self.include_folder_doc)

        if not os.path.isfile(doc_path):
            return ""

        with open(doc_path, "r") as rst_file:
            return rst_file.read()
