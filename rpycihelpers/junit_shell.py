"""Create junit file for command."""

import platform
from datetime import datetime
from typing import List, Tuple

from junit_xml import (  # type: ignore  # noqa: WPS347
    TestCase,
    TestSuite,
    to_xml_report_file,
)

from rpycihelpers.execute import execute_command


class JUnitShell(object):
    """Create JUnit from Shell command."""

    def __init__(self, suite_name: str = "") -> None:
        """Create test suite.

        :param suite_name: Name of the suite
        """
        self.suite = TestSuite(name=suite_name, hostname=platform.node())

    def run(  # noqa: WPS211
        self,
        command: str,
        class_name: str = "",
        case_name: str = "",
        category: str = "",
        output_format: str = "",
        junit_path: str = "",
    ) -> int:
        """Run command and create junit file.

        :param command: Command
        :param class_name: Name of the class name (if applicable for format)
        :param case_name: Name of test case (if applicable for format)
        :param category: Name of category (if applicable for format)
        :param output_format: Format of the command output
        :param junit_path: Output path for junit_file
        :returns: returncode
        """
        first_cmd = command.split(" ")[0]
        execute_result = execute_command(command)
        self.suite.timestamp = execute_result[3].timestamp()
        self.suite.name = self.suite.name or first_cmd
        cases = getattr(self, f"process_format_{output_format}")(
            command,
            class_name=class_name or first_cmd,
            case_name=case_name or first_cmd,
            category=category or first_cmd,
            execute_result=execute_result,
        )
        self.suite.test_cases = cases

        with open(junit_path, "w") as junit_file:
            to_xml_report_file(junit_file, [self.suite])

        return execute_result[0]

    def process_format_stdout(  # noqa: WPS211
        self,
        command: str,
        execute_result: Tuple[int, str, str, datetime, datetime],
        class_name: str,
        case_name: str,
        category: str,
    ) -> List[TestCase]:
        """Process output format in stdout / stderr.

        :param command: The command
        :param execute_result: The result from the command
        :param class_name: The class name
        :param case_name: The case name
        :param category: The category
        :returns: List with single entry for command execution.
        """
        microseconds_fact = 10e5
        duration = execute_result[4] - execute_result[3]
        duration_seconds = duration.seconds + duration.microseconds / microseconds_fact
        cases = [
            TestCase(
                name=case_name,
                category=category,
                classname=class_name,
                timestamp=execute_result[3].timestamp(),
                elapsed_sec=duration_seconds,
                stdout=execute_result[1],
                stderr=execute_result[2],
                status="success" if execute_result[0] == 0 else "failure",
            )
        ]

        if execute_result[0]:
            cases[0].failures.append(
                {
                    "output": str(
                        execute_result[1] + "\n" + execute_result[2]  # noqa: WPS336
                    ),
                    "message": f"Command {command} failed.",
                    "type": execute_result[0],
                }
            )
        return cases
