"""Non-Temporary Test folders for each test."""

import shutil
from os import chdir, getcwd, path
from re import sub
from typing import TYPE_CHECKING
from unittest import TestCase

from rpycihelpers.files import create_dir

if TYPE_CHECKING:
    from behave.model import Scenario as ScenarioType  # type: ignore
else:
    ScenarioType = any  # noqa: WPS440

DEFAULT_PATH_RESOURCES = path.join("tests", "resources")


class FolderedTest(object):
    """Create folders for tests in a base directory and chdir to it."""

    def __init__(
        self, base_path: str = "", resource_path: str = DEFAULT_PATH_RESOURCES
    ) -> None:
        """Initialize class.

        :param base_path: Directory to create folders.
        :param resource_path: Path to test resources.
        """
        self._base_path = base_path or path.join("build", "tests")
        self._base_path = self._base_path
        self._resources_path = path.join(getcwd(), resource_path)
        self._cwd_orig = getcwd()
        self._cwd = getcwd()
        create_dir(base_path)

    def copy_resource(self, resource_path: str) -> None:
        """Copy resource from resource directory into test directory.

        :param resource_path: Path of the resource
        """
        shutil.copytree(
            path.join(self._resources_path, resource_path),
            path.join(self._cwd, resource_path),
        )

    def replace_chars(self, string: str) -> str:
        """Replace all special characters by underscores.

        :param string: input string
        :returns: string with replaced chars
        """
        return sub(r"[^a-zA-Z0-9\-_]", "_", string)

    def setup(self, test_path: str) -> None:
        """(Re)create test folder and chdir to it.

        :param test_path: The testname and name for the folder
        :raises ValueError: If the testpath is the same as the exec path.
        """
        self._cwd = path.realpath(path.join(self._base_path, test_path))
        if self._cwd == self._cwd_orig:
            raise ValueError("Original path is the same as test path.")

        create_dir(self._cwd, empty=True)
        chdir(self._cwd)

    def teardown(self) -> None:
        """Change back to original cwd."""
        self._cwd = self._cwd_orig
        chdir(self._cwd)

    def __del__(self) -> None:  # noqa: WPS603
        """Teardown on delete."""
        self.teardown()


class FolderedBehaveTest(FolderedTest):
    """Implement FolderedTest for behave.

    - Create in before_all function in features/environment.py
    - Store in context.foldered_test
    - Run context.foldered_test.setup(scenario) in before_scenario function
    - Run context.foldered_test.teardown() in after_scenario function
    """

    def __init__(
        self, base_path: str = "", resource_path: str = DEFAULT_PATH_RESOURCES
    ) -> None:
        """Initialize class.

        :param base_path: The path to create the directories in.
        :param resource_path: Path to test resources.
        """
        super().__init__(base_path or path.join("build", "features"), resource_path)

    def setup(self, scenario: ScenarioType) -> None:
        """Set up directory according to context.

        :param scenario: Behave scenario context.
        """
        test_path = path.join(
            self.replace_chars(scenario.feature.name), self.replace_chars(scenario.name)
        )
        super().setup(test_path)


class FolderedPyTest(TestCase):
    """Implement FolderedTest for pytest."""

    def __init__(
        self, method_name: str = "runTest", resource_path: str = DEFAULT_PATH_RESOURCES
    ) -> None:  # no
        """Initialize class.

        :param method_name: Methodname from TestCase
        :param resource_path: Path to test resources.
        """
        super().__init__(method_name)

        self.foldered = FolderedTest(
            path.join("build", "tests", self.__class__.__name__.lower()), resource_path
        )

    def setUp(self) -> None:
        """Set up current test case."""
        super().setUp()
        self.foldered.setup(self.foldered.replace_chars(self._testMethodName))

    def tearDown(self) -> None:
        """Tear down current test case."""
        super().tearDown()
        self.foldered.teardown()
