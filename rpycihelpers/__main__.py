"""Make rpycihelpers executable by `python -m rpycihelpers`."""
from rpycihelpers import rpycihelpers

if __name__ == "__main__":  # pragma: no cover
    rpycihelpers.main()
