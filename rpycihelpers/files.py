"""File tools."""

from os import makedirs, path
from shutil import rmtree


def create_dir(directory_path: str, empty: bool = False, exist_ok: bool = True) -> None:
    """Create directory.

    :param directory_path: Path of directory
    :param empty: Clear directory if exits
    :param exist_ok: No error on exiting directory
    :raises FileExistsError: If file exists but is not a directory
    """
    if path.exists(directory_path):
        if not path.isdir(directory_path):
            raise FileExistsError(
                f"Path exits, but is not a directory: {directory_path}"
            )

        if empty:
            rmtree(directory_path)

    makedirs(directory_path, exist_ok=exist_ok)


def create_file(file_path: str, file_content: str) -> None:
    """Create or overwrite file with content.

    :param file_path: file path
    :param file_content: the content to write to the file.
    """
    with open(file_path, "w") as out_file:
        out_file.write(file_content)


def content_of_file(*file_path_parts: str) -> str:
    """Return content of file as string.

    :param file_path_parts: file path
    :returns: The file content.
    """
    with open(path.join(*file_path_parts), "r") as in_file:
        return in_file.read()
