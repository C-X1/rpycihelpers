"""Construct and run main application."""

import sys

import structlog
import typer

from rpycihelpers.cli.allure_coverage import allure_coverage  # noqa: F401
from rpycihelpers.cli.app import app
from rpycihelpers.cli.junit_command import junit_command  # noqa: F401
from rpycihelpers.cli.yaml_doc import yaml_doc  # noqa: F401


@app.callback(no_args_is_help=True)
def callback() -> None:
    """Show help when called with no arguments."""


def main() -> None:
    """Construct typer_click interface."""
    log = structlog.get_logger()

    typer_click = typer.main.get_command(app)
    try:
        typer_click()
    except Exception as exception:
        log.fatal("Caught unhandled exception")
        log.exception(exception)
        sys.exit(1)


if __name__ == "__main__":  # pragma: no cover
    main()
