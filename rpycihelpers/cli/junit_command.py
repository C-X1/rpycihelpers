"""Implement junit command."""

import sys
from typing import List

from typer import Option

from rpycihelpers.cli.app import app
from rpycihelpers.junit_shell import JUnitShell

OPTION_OUTPUT_FILE = Option(..., help="The output file")
OPTION_CLASS_NAME = Option("", help="Test class name")
OPTION_SUITE_NAME = Option("", help="Test suite name")
OPTION_CASE_NAME = Option("", help="Test case name")
OPTION_CATEGORY = Option("", help="Test category")


@app.command()
def junit_command(  # noqa: WPS211
    command: List[str],
    suite_name: str = OPTION_SUITE_NAME,
    case_name: str = OPTION_CASE_NAME,
    class_name: str = OPTION_CLASS_NAME,
    category: str = OPTION_CATEGORY,
    output_file: str = OPTION_OUTPUT_FILE,
) -> None:
    """Be an example for typer.

    :param command: The command
    :param output_file: Path of junit file.
    :param category: The category of the test
    :param suite_name: The name of the test suite
    :param case_name: The name of the test case
    :param class_name: The name of the test class
    """
    js = JUnitShell(suite_name=suite_name)

    command_string = " ".join(command)
    sys.exit(
        js.run(
            command=command_string,
            class_name=class_name,
            case_name=case_name,
            category=category,
            output_format="stdout",
            junit_path=output_file,
        )
    )
