"""Create allure json from coverage json."""

import sys

from typer import Option

from rpycihelpers.allure_coverage import AllureCoverage
from rpycihelpers.cli.app import app

OPTION_OUTPUT_PATH = Option(..., help="The output directory")
OPTION_INPUT_PATH = Option(..., help="The coverage html directory")
OPTION_ERROR_LEVEL = Option(0, help="Error level - 0 is off")
OPTION_REPORT_NAME = Option("", help="Overwrite the name of the coverage report")


@app.command()
def allure_coverage(  # noqa: WPS211
    input_path: str = OPTION_INPUT_PATH,
    output_path: str = OPTION_OUTPUT_PATH,
    error_level: int = OPTION_ERROR_LEVEL,
    report_name: str = OPTION_REPORT_NAME,
) -> None:
    """Create allure coverage report.

    :param input_path: Input file (coverage.json)
    :param output_path: Output file for allure (allure.json)
    :param error_level: If coverage is lower then there is an error
    :param report_name: The name of the coverage report
    """
    converter = AllureCoverage(error_level)
    fail = converter.convert(input_path, output_path, report_name, error_level)
    if fail:
        sys.exit(1)
