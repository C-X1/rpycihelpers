"""Add yaml_doc to CLI."""

import os
from typing import Union

from typer import Option

from rpycihelpers.cli.app import app
from rpycihelpers.yaml_doc import YamlDoc

OPTION_GENERATE_UNCOMMENTED = Option(
    default=False,
    help="Create documentation for uncommented files."
)
OPTION_FOLDER_DOC = Option(
    default=None,
    help="File to include into documentation in every folder"
)

OPTION_HEADLINE = Option(
    default="Yaml Documentation",
    help="Main headline for documentation"
)


@app.command()
def yaml_doc(  # noqa: WPS211
    input_path: str,
    output_path: str,
    headline: str = OPTION_HEADLINE,
    generate_uncommented: bool = OPTION_GENERATE_UNCOMMENTED,
    folder_doc: Union[str, None] = OPTION_FOLDER_DOC
) -> None:
    """Generate yaml doc from folder or file.

    :param input_path: The folder or file to create the documentation from
    :param output_path: The output path for file / folder
    :param generate_uncommented: Create doc file for uncommented yaml files
    :param folder_doc: File name of file to be included in folder documenation
    :param headline: The headline of the folder documenation file.
    """
    yaml_doc_obj = YamlDoc(
        generate_if_uncommented=generate_uncommented,
        include_folder_doc=folder_doc,
    )

    if os.path.isfile(input_path):  # Single file.
        yaml_doc_obj.doc_yaml_file(input_path, output_path)
    else:
        yaml_doc_obj.doc_directory(
            input_path,
            output_path,
            headline
        )
