"""Convert coverage to allure."""

import hashlib
import json
import os
import uuid
from typing import List, Tuple

from lxml import etree, html  # type: ignore  # noqa: S410

STYLE_MODIFICATION = """

aside { display: none }

#prevFileLink { display: none }

#indexLink { display: none }

#nextFileLink { display: none }

"""

FAILED = "failed"
PASSED = "passed"


class AllureCoverage(object):
    """Convert coverage to allure."""

    def __init__(self, error_level: int = 90) -> None:
        """Initialize class.

        :param error_level: Coverage level on which it is a error
        """
        self.error_level = error_level

    def convert_report(  # noqa: WPS210
        self, src_path: str, dst_path: str, style_path: str, script_path: str
    ) -> None:
        """Fix report contents for using it with allure.

        :param src_path: Original path
        :param dst_path: Ouput path
        :param style_path: Path to style file
        :param script_path: Path to script file
        """
        with open(script_path, "r") as script_file:
            script_content = script_file.read()

        with open(style_path, "r") as style_file:
            style_content = style_file.read()

        style_content += STYLE_MODIFICATION

        with open(src_path, "r") as report_file:
            report_tree = html.parse(report_file)

            style_links = report_tree.findall('//link[@rel="stylesheet"]')
            for style_link in style_links:
                parent = style_link.getparent()
                parent.remove(style_link)
                style = etree.Element("style")
                style.text = style_content
                parent.insert(0, style)

            script_tags = report_tree.findall("//script")
            for script_tag in script_tags:
                parent = script_tag.getparent()
                parent.remove(script_tag)

        with open(dst_path, "w") as dst_file:
            out_text = etree.tostring(report_tree, encoding="UTF-8")
            out_text = out_text.decode("UTF-8").replace(
                "<head>", f"<head>\n<script>\n{script_content}\n</script>"
            )
            dst_file.write(out_text)

    def report_headline_info(self, src_path: str) -> Tuple[str, int]:
        """Get the headline of the report file.

        :param src_path: The path of the file
        :returns: The file and the percentage of the coverage.
        """
        with open(src_path, "r") as report_file:
            report_tree = html.parse(report_file)

        file_path = report_tree.findall("//h1/b")[0].text_content()
        coverage = int(
            report_tree.findall('//span[@class="pc_cov"]')[0]
            .text_content()
            .replace("%", "")
        )
        return (file_path, coverage)

    def convert(  # noqa: WPS210
        self, html_path: str, output_path: str, coverage_name: str, error_level: int
    ) -> bool:
        """Convert coverage to allure report.

        :param html_path: Path of html report
        :param output_path: Path of allure output
        :param error_level: The minimum coverage per file 1-100 (<1 is off)
        :param coverage_name: Name of the coverage report
        :returns: False on passed, True on failed (error_level > coverage)
        """
        with open(os.path.join(html_path, "index.html")) as index_file:
            index_tree = html.fromstring(index_file.read())

        report_file_rows = index_tree.xpath('//tr[@class="file"]//a')
        coverage_name = coverage_name or index_tree.xpath("//title")[0].text

        os.makedirs(output_path)

        steps = []
        overall_coverage = int(
            index_tree.xpath('//span[@class="pc_cov"]')[0].text.replace("%", "")
        )
        failed = overall_coverage < self.error_level
        steps.append(
            dict(  # noqa: C408
                name=f"Overall coverage {overall_coverage}%",
                status=FAILED if failed else PASSED,
            )
        )

        for report_file_row in report_file_rows:
            report_path = os.path.join(html_path, report_file_row.attrib["href"])
            dest_path = os.path.join(output_path, os.path.basename(report_path))
            style_path = os.path.join(html_path, "style.css")
            script_path = os.path.join(html_path, "coverage_html.js")

            self.convert_report(report_path, dest_path, style_path, script_path)
            step = self.create_allure_step_for_file(report_path)
            if step["status"] == FAILED:
                failed = True
            steps.append(step)

        self.write_allure_coverage(output_path, failed, steps, coverage_name)
        return failed

    @staticmethod
    def create_allure_labels(allure_json: dict, **labels: str) -> None:
        """Set labels in allure_json dict.

        :param allure_json: The dictionary
        :param **labels: All keys to be added to the labels with their values.
        """
        allure_json["labels"] = []

        for label_name, label_value in labels.items():
            allure_json["labels"].append({"name": label_name, "value": label_value})

    def create_allure_step_for_file(self, report_path: str) -> dict:
        """Create a step for a file for the testbody.

        :param report_path: Path of the html report file
        :returns: True if failed, false otherwise
        """
        file_info = self.report_headline_info(report_path)
        coverage_name = f"{file_info[0]}: {file_info[1]}%"
        failed = file_info[1] < self.error_level

        return dict(  # noqa: C408
            status=FAILED if failed else PASSED,
            name=coverage_name,
            attachments=[
                dict(  # noqa: C408
                    name="Coverage Report",
                    type="text/html",
                    source=os.path.basename(report_path),
                )
            ],
        )

    def write_allure_coverage(
        self,
        output_path: str,
        failed: bool,
        steps: List[dict],
        coverage_name: str = "Test Coverage",
    ) -> None:
        """Create coverage report for allure.

        :param failed: If true result will be set to failed
        :param steps: List of the steps (coverage files)
        :param coverage_name: The name of the suite
        :param output_path: Path to output the json file to
        """
        allure_json = dict(  # noqa: C408
            fullName=coverage_name,
            historyId=hashlib.md5(  # noqa: S324
                coverage_name.encode("UTF-8")
            ).hexdigest(),
            uuid=str(uuid.uuid1()),
            name=coverage_name,
            status=FAILED if failed else PASSED,
            steps=steps,
        )

        AllureCoverage.create_allure_labels(
            allure_json,
            severity="trivial",
            framework="coverage",
            language="cpython3",
            suite="Coverage",
        )

        file_uuid = str(uuid.uuid1())
        file_path = os.path.join(output_path, f"{file_uuid}-result.json")

        with open(file_path, "w") as json_file:
            json_file.write(json.dumps(allure_json))
