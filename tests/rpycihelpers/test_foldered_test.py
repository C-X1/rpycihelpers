"""Test Foldered Test."""


from os import makedirs, path
from shutil import rmtree

from rpycihelpers.foldered_test import FolderedTest

BUILD_PATH = path.realpath(
    path.join(path.basename(__file__), "..", "build", "test_utils_test")
)
TEST_NAME = "test_foldered_test"
TEST_PATH = path.join(BUILD_PATH, TEST_NAME)


def test_foldered_test() -> None:
    """Test foldered test."""
    rmtree(BUILD_PATH, ignore_errors=True)
    makedirs(BUILD_PATH, exist_ok=True)
    ft = FolderedTest(BUILD_PATH)

    ft.setup(TEST_NAME)
    with open("test.txt", "w") as test_file:
        test_file.write("test")
    ft.teardown()

    assert path.exists(TEST_PATH)
    assert open(path.join(TEST_PATH, "test.txt")).read() == "test"  # noqa: WPS515
