"""Test executing shell commands."""

from datetime import datetime

from rpycihelpers.execute import execute_command


def test_execute_command() -> None:  # noqa: WPS218
    """Test executing shell commands."""
    result = None

    result = execute_command("echo out; echo err 1>&2; exit 5")

    assert result[0] == 5
    assert result[1] == "out\n"
    assert result[2] == "err\n"
    assert isinstance(result[3], datetime)
    assert isinstance(result[4], datetime)
    assert result[3] < result[4]
