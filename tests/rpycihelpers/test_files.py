"""Test file utilities."""

from os import makedirs, path

import pytest

from rpycihelpers.files import content_of_file, create_dir, create_file
from rpycihelpers.foldered_test import FolderedPyTest


class TestFiles(FolderedPyTest):
    """Test file utilities."""

    @staticmethod
    def test_create_dir() -> None:
        """Test directory creation."""
        makedirs("exist")
        makedirs("test1")
        open("test1/test.txt", "w").write("test")
        open("exist/test.txt", "w").write("test")

        create_dir("test1", exist_ok=True, empty=False)
        create_dir("exist", exist_ok=True)
        create_dir("exist", exist_ok=False, empty=True)

        with pytest.raises(FileExistsError):
            create_dir("exist", exist_ok=False)

        assert path.isfile("test1/test.txt")
        assert not path.isfile("exist/test.txt")

    @staticmethod
    def test_create_file() -> None:
        """Test creating a text file."""
        test_string = "Hello World!"
        test_file = "test.txt"

        create_file(test_file, test_string)

        assert open(test_file, "r").read() == test_string

    @staticmethod
    def test_content_of_file() -> None:
        """Test reading content of file."""
        test_string = "Hello World!"
        test_file = "test.txt"

        open(test_file, "w").write(test_string)
        result = content_of_file(test_file)

        assert result == test_string
