"""Test creating coverage report for allure."""

import os
from typing import Any, Dict

import yaml

from rpycihelpers.allure_coverage import AllureCoverage
from rpycihelpers.files import content_of_file
from rpycihelpers.foldered_test import FolderedPyTest


class TestAllureCoverage(FolderedPyTest):
    """Test creating allure coverage report."""

    def test_convert(self) -> None:
        """Test convert."""
        out_dir = "allure_out"
        converter = AllureCoverage()
        self.foldered.copy_resource("html_cov")

        converter.convert("html_cov", out_dir, "Test Coverage", 0)
        json_files = [path for path in os.listdir(out_dir) if path.endswith(".json")]
        html_files = [path for path in os.listdir(out_dir) if path.endswith(".html")]

        allure_dict = yaml.safe_load(content_of_file(out_dir, json_files[0]))

        assert len(allure_dict["steps"]) == 12  # noqa: WPS432
        assert len(html_files) == 11  # noqa: WPS432

    def test_headline_info(self) -> None:
        """Test getting headline info."""
        converter = AllureCoverage()
        self.foldered.copy_resource("html_cov")

        result = converter.report_headline_info(
            os.path.join("html_cov", "d_b6341353871b5233_foldered_test_py.html")
        )

        assert result == ("rpycihelpers/foldered_test.py", 89)

    @staticmethod
    def test_create_allure_labels() -> None:
        """Test adding labels to the allure json dictionary."""
        converter = AllureCoverage()
        allure_json: Dict[str, Any] = {}

        converter.create_allure_labels(
            allure_json,
            severity="trivial",
            feature="Test",
        )
        result = allure_json["labels"]

        assert result == [
            dict(name="severity", value="trivial"),  # noqa: C408
            dict(name="feature", value="Test"),  # noqa: C408
        ]
