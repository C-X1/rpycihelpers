"""Test shell unit."""

from rpycihelpers.files import content_of_file
from rpycihelpers.foldered_test import FolderedPyTest
from rpycihelpers.junit_shell import JUnitShell


class TestJUnitShell(FolderedPyTest):
    """Test JUnitShell."""

    def test_junit_shell_stdout(self) -> None:
        """Test processing stdout."""
        js = JUnitShell("test")

        js.run(
            command="echo out; echo err 1>&2; exit 5",
            output_format="stdout",
            junit_path="junit.xml",
        )
        result = content_of_file("junit.xml")

        assert "<system-out>out\n</system-out>" in result
        assert "<system-err>err\n</system-err>" in result
        assert '<failure type="5"' in result
