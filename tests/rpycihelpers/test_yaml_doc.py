"""Test yaml_doc."""

import os
import sys
from unittest.mock import MagicMock

import ruamel.yaml as ryaml  # noqa: WPS301

from rpycihelpers.files import content_of_file, create_file
from rpycihelpers.foldered_test import FolderedPyTest
from rpycihelpers.yaml_doc import YamlDoc


class TestYamlDoc(FolderedPyTest):
    """Test file utilities."""

    @staticmethod
    def test_create_object() -> None:
        """Test if object can be constructed."""
        results = [
            YamlDoc(),
            YamlDoc("README.rst"),
            YamlDoc(include_folder_doc="index.rst"),
        ]

        for result in results:
            assert isinstance(result, YamlDoc)

    @staticmethod
    def test_has_comment() -> None:
        """Test has_comment."""
        test_object = YamlDoc()
        test_token = ryaml.tokens.CommentToken("# Hi", 1, 2)
        test_values = (
            (None, None),
            ([], []),
            ([test_token], None),
            (None, [test_token]),
        )
        results = []

        for test_value in test_values:
            mock_map_instance = MagicMock(spec=ryaml.comments.CommentedMap)
            mock_ca_instance = MagicMock()
            mock_ca_instance.comment = test_value[0]
            mock_ca_instance.items = test_value[1]
            mock_map_instance.ca = mock_ca_instance

            results.append(test_object.has_comment(mock_map_instance))

        assert results == [False, False, True, True]

    def test_doc_yaml_file(self) -> None:
        """Test creating doc string from comment token."""
        test_object = YamlDoc()
        test_output_path = "./test1.yaml.rst"
        self.foldered.copy_resource("yaml_doc")

        test_object.doc_yaml_file("yaml_doc/test1.yaml", test_output_path)

        assert os.path.exists(test_output_path)
        file_content = open(test_output_path, "r").read()
        result = True
        expected_elements = (
            "a",
            "b",
            "c",
            "c.d",
            "c.d.e.f",
            "g",
            "g[0]",
            "g[1]",
            "g[2]",
        )

        for expected_element in expected_elements:
            head = f"**{expected_element}**"
            desc = f"This is {expected_element}"

            if head not in file_content:
                result = False
                sys.stdout.write(f"Missing head: {head}\n")

            if desc not in file_content:
                result = False
                sys.stdout.write(f"Missing description: {desc}\n")

        if "First Test" not in file_content:
            sys.stdout.write("Missing header\n")
            result = False

        assert result, f"Bad result: {file_content}"

    def test_doc_yaml_file_nc(self) -> None:
        """Test yaml file with no comments is not generated."""
        test_object = YamlDoc()
        test_output_path = "./test_nc.yaml.rst"
        self.foldered.copy_resource("yaml_doc")

        test_object.doc_yaml_file("yaml_doc/test_nc.yaml", test_output_path)

        assert not os.path.exists(test_output_path)

    def test_doc_yaml_file_nc_generate(self) -> None:
        """Test yaml file with no comments is not generated."""
        test_object = YamlDoc(generate_if_uncommented=True)
        test_output_path = "./test_nc.yaml.rst"
        self.foldered.copy_resource("yaml_doc")

        test_object.doc_yaml_file("yaml_doc/test_nc.yaml", test_output_path)

        assert os.path.exists(test_output_path)

    def test_doc_directory(self) -> None:
        """Test documenting directory."""
        test_object = YamlDoc()
        test_output_path = "output"
        headline = "Test Documentation"
        self.foldered.copy_resource("yaml_doc")

        test_object.doc_directory("yaml_doc", test_output_path, headline)

        paths = (
            "index.rst",
            "sub1",
            "sub1/index.rst",
            "sub1/test2.yaml.rst",
            "sub1/test3.yaml.rst",
            "sub1/subsub1",
            "sub1/subsub1/index.rst",
            "sub1/subsub1/test4.yaml.rst",
            "sub3/test5.yaml.rst",
            "sub3/index.rst",
        )
        assert not os.path.exists(os.path.join(test_output_path, "sub2/index.rst"))
        for path in paths:
            assert os.path.exists(os.path.join(test_output_path, path)), path
        assert headline in content_of_file(os.path.join(test_output_path, "index.rst"))

    def test_doc_yaml_file(self) -> None:
        """Test documenting yaml file."""
        content = "\n".join([
            "###",
            "### Top comment",
            "###",
            "a: 5 ### This is a",
            "b: 5 ### This is b",
            "c: 5 ### This is c",
            "g: ### This is g-array",
            "",
            "  - ### This is g0",
            "    g0: 5",
            "    a: 5",
            "",
            "  - ### This is g1",
            "    g1: 5",
        ])
        test_object = YamlDoc()
        test_file_path = "test.yaml"
        out_file_path = "out.yaml"
        create_file(test_file_path, content)

        test_object.doc_yaml_file(test_file_path, out_file_path)

        assert os.path.isfile(out_file_path)
        result = content_of_file(out_file_path)
        expected_once = (
            "Top comment",
            "This is a",
            "This is b",
            "This is c",
            "This is g-array",
            "This is g0",
            "This is g1"
        )
        for expected in expected_once:
            assert result.count(expected) == 1, result
